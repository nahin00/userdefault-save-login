//
//  HomeViewController.swift
//  UserDefault save login
//
//  Created by Nahin Ahmed on 18.07.19.
//  Copyright © 2019 NAhmed. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func logoutBtnAction(_ sender: UIButton) {
        
        UserDefaults.standard.removeObject(forKey: "email")
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let navVc = UINavigationController(rootViewController: vc)
        let share = UIApplication.shared.delegate as? AppDelegate
        share?.window?.rootViewController = navVc
        share?.window?.makeKeyAndVisible()
        
        dismiss(animated: true, completion: nil)
    }
}
