//
//  LoginViewController.swift
//  UserDefault save login
//
//  Created by Nahin Ahmed on 18.07.19.
//  Copyright © 2019 NAhmed. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleScreenTap(sender:)))
        
        self.view.addGestureRecognizer(tapGesture)
        
        emailTF.delegate = self
        passwordTF.delegate = self
    }
    
    @objc func handleScreenTap(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func submitBtnAction(_ sender: UIButton) {
        
        guard let email = emailTF.text else {
            return
        }
        
        guard  let password = passwordTF.text  else {
            return
        }
        
        if email.count == 0 || password.count == 0 {
            
            let alertController = UIAlertController(title: "Error", message: "Please enter a valid email and password!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        
        } else {
            
            UserDefaults.standard.set(email, forKey: "email")
            
            let dc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.present(dc, animated: true, completion: nil)
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        emailTF.resignFirstResponder()
        passwordTF.resignFirstResponder()
        return true
    }
}
